# Node-Red Service GraphQL
Implementation for [Node-RED](https://nodered.org/) to connect with a GraphQL endpoint.

## Installation
To install node-red-module-graphql use this command

`npm i https://gitlab.com/ambtec/node-red-module-graphql.git`

## Composition
The GraphQL implementation is made with
* 1 GraphQL configuration node

## Usage
Place a GraphQL configuration node and attach a http response node to it (POST). \
Then copy and paste your GraphQL configuration into the input form of the GraphQL configuration node.

## License
MIT
