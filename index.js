module.exports = function (RED) {
  const unquoteKeys = function (string) {
    return string.replace(/\"([^(\")"]+)\":/g, "$1:");
  };

  /**
   * Handle graphql config event
   *
   * @param {*} config
   */
  function graphqlConfig(config) {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on("input", function (msg) {
      if (config.configuration) {
        const regexWhitespaceNewLine = new RegExp("(\\s|\\n)+", "g");
        const regexParameter = new RegExp("\\(([^)]+)\\)");
        let message = unquoteKeys(JSON.stringify(msg.payload));
        message = message.substring(1, message.length - 1);
        let parsedConfiguration = config.configuration.replace(
          regexWhitespaceNewLine,
          " "
        );

        const parameterArea = regexParameter.exec(parsedConfiguration);
        if (parameterArea) {
          parsedConfiguration = parsedConfiguration.replace(
            parameterArea[0],
            `(${message})`
          );
        }

        msg.payload = JSON.stringify({
          query: parsedConfiguration,
          variables: {},
        });

        if (!msg.headers) {
          msg.headers = {};
        }
        msg.headers["content-type"] = "application/json";
      }
      node.send(msg);
    });
  }

  RED.nodes.registerType("graphql-config", graphqlConfig);
};
